<?php

namespace App\Http\Controllers;

use App\Serie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SerieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Serie[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return view('series.serieList', ['series' => Serie::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('series.serieCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'title'          => 'required | string',
            'description'    => 'string'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

        } else {
            $serie = new Serie;
            $serie->title           = $request->input('title');
            $serie->description     = $request->input('description');
            $serie->save();

            return redirect('/series');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Serie  $serie
     * @return \Illuminate\Http\Response
     */
    public function show(Serie $serie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Serie  $serie
     * @return \Illuminate\Http\Response
     */
    public function edit(Serie $serie)
    {
        return view('series.serieEdit', ['serie' => $serie]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Serie $serie
     * @return Serie[]|\Illuminate\Database\Eloquent\Collection
     */
    public function update(Request $request, Serie $serie)
    {
        $rules = array(
            'title'          => 'required | string',
            'description'    => 'string'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

        } else {
            $serie->title           = $request->input('title');
            $serie->description     = $request->input('description');
            $serie->save();

            return redirect('/series');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Serie $serie
     * @return void
     */
    public function destroy(Serie $serie)
    {
        Serie::findOrFail($serie->id)->delete();
        return redirect('/series');
    }
}
