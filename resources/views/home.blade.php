@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    <div class="row justify-content-around">
                    <a href="/series" class="btn btn-primary">Accéder à mes séries</a>
                    <a href="/series/create" class="btn btn-primary">Ajouter une série</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
