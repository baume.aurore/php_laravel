<div class="card">
    <div class="card-body row">
        <div class="col-md-10">
            <h2 class="title">{{ $serie->title }}</h2>
            <div class="description">{{ $serie->description }}</div>
        </div>
        <div class="col-md-2">
            <a class="btn btn-primary" href="/series/{{$serie->id}}/edit" role="button">Editer</a>
            <form action="/series/{{$serie->id}}" method="post" class="form-serie">
                @method('delete')
                @csrf
                <button class=" btn btn-danger" type="submit">Delete</button>
            </form>
        </div>
    </div>
</div>
