@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="/series/{{$serie->id}}" method="post" class="form-serie">
            @method('put')
            @csrf
            <div class="form-serie">
                <label for="title">Titre de la série: </label>
                <input type="text" name="title" id="title" required value="{{ $serie->title }}">
            </div>
            <div class="form-example">
                <label for="description">Description: </label>
                <input type="text" name="description" id="description" value="{{ $serie->description }}">
            </div>
            <div class="form-example">
                <input type="submit" value="Editer">
            </div>
        </form>
    </div>
@endsection
