@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($series as $serie)
            @component('series.components.series', ['serie' => $serie])
            @endcomponent
        @endforeach
    </div>
@endsection
