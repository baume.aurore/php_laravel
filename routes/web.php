<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/series', 'SerieController@index')->name('series');
Route::get('/series/create', 'SerieController@create')->name('series_form_create');
Route::post('/series', 'SerieController@store')->name('series_store');

Route::get('/series/{serie}/edit', 'SerieController@edit')->name('series_form_edit');
Route::put('/series/{serie}', 'SerieController@update')->name('series_edit');

Route::delete('/series/{serie}', 'SerieController@destroy')->name('series_delete');
